![Project Avatar](repo-icon.png)

# Base Modpack

The foundation for any of my modpacks. This includes all necessary mods that should be in all of my modpacks.

[Issue Board](https://gitlab.com/cody-bolling/games/minecraft/base-modpack/-/boards)
